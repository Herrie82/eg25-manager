/*
 * Copyright (C) 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "gpio.h"

#include <unistd.h>

/* Those defines are used for legacy config files only */
#define GPIO_CHIP1_LABEL "1c20800.pinctrl"
#define GPIO_CHIP2_LABEL "1f02c00.pinctrl"
#define MAX_GPIOCHIP_LINES 352

enum {
    GPIO_OUT_DTR = 0,
    GPIO_OUT_PWRKEY,
    GPIO_OUT_RESET,
    GPIO_OUT_APREADY,
    GPIO_OUT_DISABLE,
    GPIO_OUT_COUNT
};

enum {
    GPIO_IN_STATUS = 0,
    GPIO_IN_COUNT
};

static char *gpio_out_names[] = {
    "dtr",
    "pwrkey",
    "reset",
    "apready",
    "disable",
};

static char *gpio_in_names[] = {
    "status",
};

int gpio_sequence_poweron(struct EG25Manager *manager)
{
    gpiod_line_set_value(manager->gpio_out[GPIO_OUT_PWRKEY], 1);
    sleep(1);
    gpiod_line_set_value(manager->gpio_out[GPIO_OUT_PWRKEY], 0);

    g_message("Executed power-on/off sequence");

    return 0;
}

int gpio_sequence_shutdown(struct EG25Manager *manager)
{
    gpiod_line_set_value(manager->gpio_out[GPIO_OUT_DISABLE], 1);
    gpio_sequence_poweron(manager);

    g_message("Executed power-off sequence");

    return 0;
}

int gpio_sequence_suspend(struct EG25Manager *manager)
{
    gpiod_line_set_value(manager->gpio_out[GPIO_OUT_APREADY], 1);

    g_message("Executed suspend sequence");

    return 0;
}

int gpio_sequence_resume(struct EG25Manager *manager)
{
    gpiod_line_set_value(manager->gpio_out[GPIO_OUT_APREADY], 0);

    g_message("Executed resume sequence");

    return 0;
}

int gpio_sequence_wake(struct EG25Manager *manager)
{
    if (gpiod_line_get_value(manager->gpio_out[GPIO_OUT_DTR])) {
        gpiod_line_set_value(manager->gpio_out[GPIO_OUT_DTR], 0);

        /* Give the modem 200ms to wake from soft sleep */
        usleep(200000);

        g_message("Executed soft wake sequence");
    }

    return 0;
}

int gpio_sequence_sleep(struct EG25Manager *manager)
{
    gpiod_line_set_value(manager->gpio_out[GPIO_OUT_DTR], 1);
    g_message("Executed soft sleep sequence");

    return 0;
}

struct gpiod_line *gpio_get_output_line(struct EG25Manager *manager, int chip, int line)
{
    struct gpiod_line *gpio_line;

    gpio_line = gpiod_chip_get_line(manager->gpiochip[chip], line);
    if (!gpio_line)
        return NULL;

    if (gpiod_line_request_output(gpio_line, "eg25manager", 0) < 0) {
        gpiod_line_release(gpio_line);
        return NULL;
    }

    return gpio_line;
}

struct gpiod_line *gpio_get_input_line(struct EG25Manager *manager, int chip, int line)
{
    struct gpiod_line *gpio_line;

    gpio_line = gpiod_chip_get_line(manager->gpiochip[chip], line);
    if (!gpio_line)
        return NULL;

    if (gpiod_line_request_input(gpio_line, "eg25manager") < 0) {
        gpiod_line_release(gpio_line);
        return NULL;
    }

    return gpio_line;
}

int gpio_init(struct EG25Manager *manager, toml_table_t *config[])
{
    int i;
    toml_table_t *gpio_config[EG25_CONFIG_COUNT];

    for (i = 0; i < EG25_CONFIG_COUNT; i++)
        gpio_config[i] = config[i] ? toml_table_in(config[i], "gpio") : NULL;

    if (!gpio_config[EG25_CONFIG_SYS])
        g_error("Default config file lacks the 'gpio' section!");

    /* 
     * The system config could have the `chips` key, but the user one
     * could still use the old format! In order to avoid problems, we
     * should use the new format only if:
     *   - there's no user config file
        or
     *   - the user config file contains the `chips` key
     * Otherwise we might start parsing the system config with the new
     * format, but error out if user config overrides gpios using the
     * old format
     */
    if (!gpio_config[EG25_CONFIG_USER] || toml_array_in(gpio_config[EG25_CONFIG_USER], "chips"))
    {
        int numchips;
        toml_array_t *chipslist = NULL;

        config_get_array(gpio_config, "chips", &chipslist);
        numchips = toml_array_nelem(chipslist);
        if (numchips > 2)
            g_error("Requesting too many GPIO chips!");

        for (i = 0; i < numchips; i++) {
            toml_datum_t data = toml_string_at(chipslist, i);
            if (!data.ok)
                continue;
            manager->gpiochip[i] = gpiod_chip_open_by_label(data.u.s);
            if (!manager->gpiochip[i])
                g_error("Unable to find GPIO chip '%s'", data.u.s);
        }

        for (i = 0; i < GPIO_OUT_COUNT; i++) {
            toml_table_t *table;
            toml_datum_t chip, line;
            if (!config_get_table(gpio_config, gpio_out_names[i], &table))
                g_error("Unable to get config for output GPIO '%s'", gpio_out_names[i]);

            chip = toml_int_in(table, "chip");
            if (!chip.ok || chip.u.i < 0 || chip.u.i > 2)
                g_error("Wrong chip ID for output GPIO '%s'", gpio_out_names[i]);

            line = toml_int_in(table, "line");
            if (!line.ok || line.u.i < 0 || line.u.i > gpiod_chip_num_lines(manager->gpiochip[chip.u.i]))
                g_error("Wrong line ID for output GPIO '%s'", gpio_out_names[i]);

            manager->gpio_out[i] = gpio_get_output_line(manager, chip.u.i, line.u.i);
            if (!manager->gpio_out[i])
                g_error("Unable to get output GPIO %d", i);
        }

        for (i = 0; i < GPIO_IN_COUNT; i++) {
            toml_table_t *table;
            toml_datum_t chip, line;
            if (!config_get_table(gpio_config, gpio_in_names[i], &table))
                g_error("Unable to get config for input GPIO '%s'", gpio_in_names[i]);

            chip = toml_int_in(table, "chip");
            if (!chip.ok || chip.u.i < 0 || chip.u.i > 2)
                g_error("Wrong chip ID for input GPIO '%s'", gpio_in_names[i]);

            line = toml_int_in(table, "line");
            if (!line.ok || line.u.i < 0 || line.u.i > gpiod_chip_num_lines(manager->gpiochip[chip.u.i]))
                g_error("Wrong line ID for input GPIO '%s'", gpio_in_names[i]);

            manager->gpio_in[i] = gpio_get_input_line(manager, chip.u.i, line.u.i);
            if (!manager->gpio_in[i])
                g_error("Unable to get input GPIO %d", i);
        }
    } else {
        guint offset, chipidx, gpio_idx;

        /* Legacy config file, only used on the OG PinePhone */
        manager->gpiochip[0] = gpiod_chip_open_by_label(GPIO_CHIP1_LABEL);
        if (!manager->gpiochip[0])
            g_error("Unable to open GPIO chip " GPIO_CHIP1_LABEL);

        manager->gpiochip[1] = gpiod_chip_open_by_label(GPIO_CHIP2_LABEL);
        if (!manager->gpiochip[1])
            g_error("Unable to open GPIO chip " GPIO_CHIP2_LABEL);

        for (i = 0; i < GPIO_OUT_COUNT; i++) {
            if (!config_get_uint(gpio_config, gpio_out_names[i], &gpio_idx))
                g_error("Unable to get config for output GPIO '%s'", gpio_out_names[i]);

            if (gpio_idx < MAX_GPIOCHIP_LINES) {
                offset = gpio_idx;
                chipidx = 0;
            } else {
                offset = gpio_idx - MAX_GPIOCHIP_LINES;
                chipidx = 1;
            }

            manager->gpio_out[i] = gpio_get_input_line(manager, chipidx, offset);
            if (!manager->gpio_out[i])
                g_error("Unable to get output GPIO %d", i);
        }

        for (i = 0; i < GPIO_IN_COUNT; i++) {
            if (!config_get_uint(gpio_config, gpio_in_names[i], &gpio_idx))
                continue;

            if (gpio_idx < MAX_GPIOCHIP_LINES) {
                offset = gpio_idx;
                chipidx = 0;
            } else {
                offset = gpio_idx - MAX_GPIOCHIP_LINES;
                chipidx = 1;
            }

            manager->gpio_in[i] = gpio_get_input_line(manager, chipidx, offset);
            if (!manager->gpio_in[i])
                g_error("Unable to get input GPIO %d", i);
        }
    }

    return 0;
}

gboolean gpio_check_poweroff(struct EG25Manager *manager, gboolean keep_down)
{
    if (manager->gpio_in[GPIO_IN_STATUS] &&
        gpiod_line_get_value(manager->gpio_in[GPIO_IN_STATUS]) == 1) {

        if (keep_down && manager->gpio_out[GPIO_OUT_RESET]) {
            // Asserting RESET line to prevent modem from rebooting
            gpiod_line_set_value(manager->gpio_out[GPIO_OUT_RESET], 1);
        }

        return TRUE;
    }

    return FALSE;
}

void gpio_destroy(struct EG25Manager *manager)
{
    int i;

    for (i = 0; i < GPIO_OUT_COUNT; i++) {
        if (manager->gpio_out[i])
            gpiod_line_release(manager->gpio_out[i]);
    }

    for (i = 0; i < GPIO_IN_COUNT; i++) {
        if (manager->gpio_in[i])
            gpiod_line_release(manager->gpio_in[i]);
    }

    if (manager->gpiochip[0])
        gpiod_chip_close(manager->gpiochip[0]);
    if (manager->gpiochip[1])
        gpiod_chip_close(manager->gpiochip[1]);
}
