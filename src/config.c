/*
 * Copyright (C) 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "toml.h"

gboolean config_get_bool(toml_table_t **config, const gchar *key, gboolean *result)
{
    toml_datum_t value = { .ok = 0 };

    if (config[EG25_CONFIG_USER])
        value = toml_bool_in(config[EG25_CONFIG_USER], key);
    if (!value.ok)
        value = toml_bool_in(config[EG25_CONFIG_SYS], key);
    if (value.ok && result)
        *result = value.u.b;

    return !!value.ok;
}

gboolean config_get_int(toml_table_t **config, const gchar *key, gint *result)
{
    toml_datum_t value = { .ok = 0 };

    if (config[EG25_CONFIG_USER])
        value = toml_int_in(config[EG25_CONFIG_USER], key);
    if (!value.ok)
        value = toml_int_in(config[EG25_CONFIG_SYS], key);
    if (value.ok && result)
        *result = value.u.i;

    return !!value.ok;
}

gboolean config_get_uint(toml_table_t **config, const gchar *key, guint *result)
{
    gint value;
    gboolean found;

    found = config_get_int(config, key, &value);
    if (found) {
        if (value <= 0 || value >= G_MAXUINT) {
            g_message("Value out of range for [%s], discarding", key);
            found = FALSE;
        }
    }

    if (found && result)
        *result = (guint) value;

    return found;
}

gboolean config_get_string(toml_table_t **config, const gchar *key, gchar **result)
{
    toml_datum_t value = { .ok = 0 };

    if (config[EG25_CONFIG_USER])
        value = toml_string_in(config[EG25_CONFIG_USER], key);
    if (!value.ok)
        value = toml_string_in(config[EG25_CONFIG_SYS], key);
    if (value.ok && result)
        *result = value.u.s;

    return !!value.ok;
}

gboolean config_get_array(toml_table_t **config, const gchar *key, toml_array_t **result)
{
    toml_array_t *array = NULL;

    if (config[EG25_CONFIG_USER])
        array = toml_array_in(config[EG25_CONFIG_USER], key);
    if (!array)
        array = toml_array_in(config[EG25_CONFIG_SYS], key);
    if (array && result)
        *result = array;

    return !!array;
}

gboolean config_get_table(toml_table_t **config, const gchar *key, toml_table_t **result)
{
    toml_table_t *table = NULL;

    if (config[EG25_CONFIG_USER])
        table = toml_table_in(config[EG25_CONFIG_USER], key);
    if (!table)
        table = toml_table_in(config[EG25_CONFIG_SYS], key);
    if (table && result)
        *result = table;

    return !!table;
}
